# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday=True, is_sunny=True):
    gear = []
    if is_workday and is_sunny:
        gear.append("Bring laptop")
    elif is_workday and not is_sunny:
        gear.append("Bring umbrella")
    elif not is_workday and is_sunny:
        gear.append("Bring surfboard")
    return gear


gear_for_day(True, True)
