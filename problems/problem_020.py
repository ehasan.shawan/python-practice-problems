# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):

    percentage = (members_list * .50)
    if attendees_list >= percentage:
        print("Attendence is more than 50%")
    else:
        print("Attendance is less than 50%")


has_quorum(34, 70)
